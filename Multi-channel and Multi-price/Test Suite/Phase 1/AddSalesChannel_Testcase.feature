Scenario: Display Dropdown list
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I click dropdown list sales channels
    Then I expect I see Grab/Get/Foodpanda/Robinhood/Eatable

Scenario: Custom sales channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I click + button
    Then I see custom field in pop-up
    And I click x button
    Then I see dropdown list sales channels

Scenario: Choose a type NO GP
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I choose a type NO GP
    Then I expect Percentage field hide

Scenario: Choose a type GP
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I choose a type GP
    Then I expect percentage field 
    And I can input number on percentage field 

Scenario: Default type of service
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    Then I expect all service are selected

Scenario: Select only type delivery
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I select only type delivery
    Then I expect service delivery is selected

Scenario: Select only type pickup
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I select only type pickup
    Then I expect service pickup is selected

Scenario: Select type pickup and delivery
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    And I select type pickup and delivery
    Then I expect service pickup and delivery are selected

Scenario: Add sales chanel Grab and GP 20% type delivery 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button 
    Then I expect I see pop-up add sales channel
    And I click dropdown list sales channels
    And I choose Grab in dropdown
    And I choose type GP and input '20' on percentage field  
    And I choose service 'delivery'
    And I click save button
    Then I expect I see sales chanel 'Grab' on page 
    And I see tag 'GP' and correct icon

Scenario: Add sales chanel FoodPanda and GP 10% type pickup 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button 
    Then I expect I see pop-up add sales channel
    And I click dropdown list sales channels
    And I choose 'FoodPanda' in dropdown
    And I choose type GP and input '10' on percentage field  
    And I choose service 'pickup'
    And I click save button
    Then I expect I see sales chanel 'FoodPanda' on page 
    And I see tag 'GP' and correct icon

Scenario: Add sales chanel custom and NO GP all service 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button 
    Then I expect I see pop-up add sales channel
    And I click custom channel name 'xxxx'
    And I choose type NO GP 
    And I choose service 'all service'
    And I click save button
    Then I expect I see sales chanel 'xxxx' on page 
    And I see tag 'NO GP' and correct icon


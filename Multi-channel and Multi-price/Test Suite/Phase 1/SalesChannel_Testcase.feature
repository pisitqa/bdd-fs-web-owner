Scenario: Default Sales Channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    Then I expect I see default sales channel

Scenario: Click add Sales Channel button
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I click add sales channel button
    Then I expect I see pop-up add sales channel

Scenario: Select a sales channel 'xxxx'
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I select sales channel 'xxxx'
    Then I expect I see correct data of sales channel

Scenario: Stores have more than one branch
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And I want to choose branch 'xxx'
    Then I expect I see correct data of branch

Scenario: Sales channel have GP
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Default sales channel 'xxx' have GP
    Then I expect I see tag GP on sales chaanel 'xxx'

Scenario: Sales channel have no GP
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Default sales channel 'xxx' have no GP
    Then I expect I see tag NO GP on sales chaanel 'xxx'

Scenario: Display the number of sales channels menu
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Default sales channel 'xxx' have 'xxx' menu
    Then I expect I see correct number of sales channels menu

Scenario: Sales channel icon
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    Then I expect I see default sales channel and custom sales chanel
    And I expect each sales channel icon is correct
Scenario: Default select menus
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    Then I expect the first time a menu is added, all menus and categories must be selected

Scenario: Click cancel button on pop-up add menu case Empty menu on page
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I click cancel button
    Then I expect I see empty state

Scenario: Click cancel button on pop-up add menu case have menus on page
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I click cancel button
    Then I expect that I will see a menu that has been selected

Scenario: Click save button on pop-up add menu case Empty menu on page
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I add menu 'xxx'
    And I click save button
    Then I expect I see the menu I just selected

Scenario: Click save button on pop-up add menu case have menus on page
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I add menu 'xxx'
    And I click save button
    Then I expect I see correct menus

Scenario: Display category 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    Then I expect I see correct category

Scenario: Choose all category 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I click all category button 
    Then I expect I see all menus are selected

Scenario: Display menu
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    Then I expect I see correct menu

Scenario: Choose all menu 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I click category 'xxx'
    And I click all menu button 
    Then I expect I see all menus in category are selected

Scenario: Choose some menu
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I add menu 'xxx'
    Then I expect I see correct menus

Scenario: Search menu 
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    Then I expect I'm on detail chanel page
    And I click add menu button
    And I search menu 'xxx' in category 'xxx'
    Then I see menu 'xxx'

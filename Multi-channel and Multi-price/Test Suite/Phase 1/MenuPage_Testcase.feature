Scenario: No choose all sales channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Menu
    And I select menu 'xxx'
    And I unselect all sales channel
    Then I expect menu not show all channel

Scenario: Choose all sales channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Menu
    And I select menu 'xxx'
    And I select all sales channel
    Then I expect menu show all channel

Scenario: Choose some sales channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Menu
    And I select menu 'xxx'
    And I select 'xxxx' sales channel
    Then I expect menu show 'xxxx' channel

Scenario: Edit menu price
     Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Menu
    And I select menu 'xxx'
    And I select 'xxxx' sales channel
    And I want to edit menu price
    Then I expect Actual sale price 'xxx' = 'xxx'


Scenario: Default sales channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    And I click setting button
    Then I expect delete button hide

Scenario: Delete custom sales channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    And I click setting button
    And I want to delete sales channel
    Then I expect I see pop-up confrim delete chanel
    And I click OK
    Then Chanel is removed

Scenario: Edit Channel
    Given I Open web owner by user 'xxxx'
    When I select Menu Management on menu side
    And I select sub menu Sales Chanel
    And Select sales chanel 'xxx'
    And I click setting button
    And I want to edit % GP 'xx' %
    And I click Save
    Then I expect correct data